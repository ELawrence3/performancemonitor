﻿using System;
using Microsoft.VisualStudio.TestTools.LoadTesting;
using System.Net.Mail;
using System.Collections.Generic;
//using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace EmailAlerts
{
    public class EmailAlerts : ILoadTestPlugin
    {
        static LoadTest myLoadTest;
        static List<string> failed = new List<string>();
        const string connstr = @"Data Source=SQL2012-2A\ITS_APPS_TEST;Initial Catalog=LoadTestRepository;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";
       static DataTable emailList = new DataTable();
        public static string adminEmail = "e.lawrence3@snhu.edu";

        public void Initialize(LoadTest loadTest)
        {
            myLoadTest = loadTest;

            myLoadTest.TestFinished += new
                EventHandler<TestFinishedEventArgs>(myLoadTest_TestFinished);

            myLoadTest.LoadTestFinished += new
                EventHandler(myLoadTest_LoadTestFinished);
            
        }

        void myLoadTest_TestFinished(object sender, TestFinishedEventArgs e)
        {
            if (e.Result.Passed==false)
            {
                failed.Add(e.ScenarioName);
            }            
        }
        public static void myLoadTest_LoadTestFinished(object sender, EventArgs e )
        {
            string locations="";
            
            //for(int i=0;i< failed.Count; i++)
            //{
                //locations += failed[i];
                if(failed.Count>0)
                    locations += failed[0];
            //}

            if (failed.Count > 0)
            {             
                try
                {
                    MailMessage MyMail = new MailMessage();
                    MyMail.From = new MailAddress("PerfMon@snhu.edu");

                    if (emailList.Rows.Count==0)
                    {
                        try
                        {
                            SqlConnection connection = new SqlConnection(connstr);
                            connection.Open();

                            SqlCommand check = new SqlCommand(@"SELECT [emailList]
                                                                  FROM [LoadTestRepository].[dbo].[TestStepDetails]
                                                                  where LoadTestName = '" + myLoadTest.Name + "'", connection);

                            SqlDataReader reader = check.ExecuteReader();
                            
                            emailList.Load(reader);

                            foreach (DataRow r in emailList.Rows)
                            {
                                foreach(var address in r[0].ToString().Split(new [] {";"}, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    MyMail.To.Add(address);
                                }
                                
                            }

                        }
                        catch (Exception)
                        {
                            //do nothing
                        }
                    }


                    MyMail.Subject = myLoadTest.Name + " " + locations;
                    MyMail.Body = "Performance Monitor Reported " + myLoadTest.Name + " failed from: " + locations + emailList;

                    MyMail.Body += Environment.NewLine + Environment.NewLine + "If you wish to change your subscription settings please visit http://vs-control/PerfMonAlerts/Home/Alerts"
                    + Environment.NewLine + "For additional assistance you may contact the site's administrator directly at " + adminEmail + "."
                    + Environment.NewLine + Environment.NewLine + "All the best," + Environment.NewLine + Environment.NewLine + "The Southern New Hampshire University Quality Assurance Team";


                    SmtpClient MySmtpClient = new SmtpClient("smtp.snhu.edu");
                    MySmtpClient.Send(MyMail);
                }

                catch (SmtpException ex)
                {
                    // MessageBox.Show(ex.InnerException.Message +
                    //    ".\r\nMake sure you have a valid SMTP.", "LoadTestPlugin", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                }
            }
        }
    }
}