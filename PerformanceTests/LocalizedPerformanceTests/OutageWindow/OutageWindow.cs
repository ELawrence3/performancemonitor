﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Microsoft.VisualStudio.TestTools.WebTesting;


namespace OutageWindow
{
    [Description("This plugin will allow the setting of outage windows. The test will automatically pass during these windows.")]
    public class OutageWindow : WebTestPlugin
    {
        [Description("Set the start datetime for the outage window")]
        [DefaultValue("01/28/2017 14:31")]        
        public string OutageBegins { get; set; }
        [Description("Set the ending datetime for the outage window")]
        [DefaultValue("01/28/2017 02:31")]
        public string OutageEnds { get; set; }


        public override void PreWebTest(object sender, PreWebTestEventArgs e)
        {
            
            

            DateTime now = DateTime.Now;
            DateTime start = DateTime.Parse(OutageBegins);
            DateTime end = DateTime.ParseExact(OutageEnds, "MM/dd/yyyy HH:mm", null);

            if(start < end)
            {
                if(start <= now && now <= end)
                {
                    // WebTestCondition Outcome.Pass;
                    e.WebTest.Outcome = Outcome.Pass;
                    e.WebTest.Stop();
                    e.WebTest.AddCommentToResult("This is an expected outage window");
                    //throw new ApplicationException("This is an expected outage window");
                }
            }
            else
            {
                throw new ApplicationException("OutageStarts must come before OutageEnds");
            }
            base.PreWebTest(sender, e);
            //For SSL
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

            //we wire up the callback so we can override  behavior and force it to accept the cert
            //   ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateValidationCB;



            //let them know we made changes to the service point manager
            /* e.WebTest.AddCommentToResult(
            this.ToString() + start + " " + end + " " + now + " has made the following modification-> ServicePointManager.SecurityProtocol set to use SSLv3 in WebTest Plugin.");*/
        }
    }

}
