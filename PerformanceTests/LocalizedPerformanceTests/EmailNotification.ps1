﻿function submit_report_smtp{
    param($report)
    trap{return 1}
    $smtp_client = New-Object system.Net.Mail.SmtpClient
    $smtp_client.Host = 'smtp.snhu.edu'
    $smtp_client.send('QA_t.testy@snhu.edu', 'e.lawrence3@snhu.edu', 'Performance Monitor Detected an Error',$report)
    return 0
}

$to = Get-Date ((Get-Date)+(New-TimeSpan -Minutes 1)) -format "yyyy-MM-dd HH:mm:ss"
$from = Get-Date ((Get-Date)-(New-TimeSpan -Minutes 15)) -format "yyyy-MM-dd HH:mm:ss"
$results = Invoke-Sqlcmd -Query "select TRe.LoadTestRunId,TRu.LoadTestName,TRo.ScenarioName,TRa.AgentName,TRu.StartTime,TRE.PageId,TRx.RequestUri,TRe.ResponseTime,TRe.Outcome from [LoadTestRepository].[dbo].[LoadTestPageDetail] TRe inner join [LoadTestRepository].[dbo].[LoadTestRun] TRu on TRe.LoadTestRunId = TRu.LoadTestRunId inner join [LoadTestRepository].[dbo].[WebLoadTestRequestMap] TRx on Tre.LoadTestRunId = TRx.LoadTestRunId and TRe.PageId = TRx.RequestId inner join [LoadTestRepository].[dbo].[LoadTestTestDetail] TRd on Tre.LoadTestRunId = TRd.LoadTestRunId inner join [LoadTestRepository].[dbo].[LoadTestScenario] TRo on Trd.LoadTestRunId = TRo.LoadTestRunId and TRo.ScenarioId = TRd.TestCaseId and TRe.TestDetailId = Trd.TestCaseId inner join [LoadTestRepository].[dbo].[LoadTestRunAgent] TRa on Trd.LoadTestRunId = TRa.LoadTestRunId and TRa.AgentId = TRd.AgentId and TRe.LoadTestRunId in (select LoadTestRunId from [LoadTestRepository].[dbo].[LoadTestRun] where Outcome = 'Completed') where TRe.Outcome='1' and TRu.StartTime BETWEEN '$from' AND '$to'" -ServerInstance "SQL2012-2A\ITS_APPS_TEST"
$results.Count
$errors = $(foreach ($line in $results) {'Performance monitoring has reported an error when connecting to ' +  $line.LoadTestName + ' from ' + $line.ScenarioName + '. This error was reported at ' + $line.StartTime}) | sort | Get-Unique
$errors.Count
foreach ($e in $errors) {submit_report_smtp($e)}