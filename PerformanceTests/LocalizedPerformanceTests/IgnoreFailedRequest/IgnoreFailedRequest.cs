﻿using Microsoft.VisualStudio.TestTools.WebTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IgnoreFailedRequest
{
    public class IgnoreFailedRequest : WebTestRequestPlugin
    {
        public override void PostRequest(object sender, PostRequestEventArgs e)
        {
            if (e.Request.Outcome == Outcome.Fail /*|| e.Request.DependentRequests[0].Outcome == Outcome.Fail */||  !e.Response.StatusCode.ToString().Equals("OK"))
            {
                e.Request.Outcome = Outcome.Pass;
               // e.Request.DependentRequests[0].Outcome = Outcome.Pass;
            }
            else
            {
                e.WebTest.Context.Add(new KeyValuePair<string, object>("StopLooping", ""));
            }
        }
    }

}
