﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Data;


namespace SNHU.QA.PerfMon_TestRunner
{    
    class Program
    {       
        const int deleteTestsAfter = 14; //days.Tests older than this will be automatically deleted
        const int scanForErrorsRange = 1; //minutes.  Sets frequency and range of error checking and alert
        const int testRunFrequency = 15; //minutes. sets frequency of test suite run.
        const int deleteOldTestsFrequency = 24; //hours. How often to scan for and remove tests older than "deleteTestsAfter".
        const int cleanDBFreqency = 24; //hours. How often old tests are removed from the central database
        const int removeTestFromDBafter = 31; //days. How long to keep tests in the central database before removal.
        const string connstr = @"Data Source=SQL2012-2A\ITS_APPS_TEST;Initial Catalog=LoadTestRepository;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";

        const string logFile = @"\\vs-control\E$\TestResults\log.txt";
        // static string path = @"\\vs-control\C$\Users\adm_elawrence3\Documents\Visual Studio 2015\Projects\LocalizedPerformanceTests\";
         static string path = @"C:\Users\adm_elawrence3\OneDrive - SNHU\Documents\Visual Studio 2015\Projects\LocalizedPerformanceTests\";
        static void Main(string[] args)
        {
          //  log("Test Runner Application has Started Successfully");
            

            System.Threading.Timer timer = new System.Threading.Timer(Callback, null, TimeSpan.Zero, TimeSpan.FromMinutes(testRunFrequency));
       /*     if (System.Environment.MachineName == "VS-CONTROL")
            {
                System.Threading.Timer checkForErrorsTimer = new System.Threading.Timer(o => checkForErrors(DateTime.Now.Subtract(new TimeSpan(0, scanForErrorsRange, 0)).ToString("yyyy-MM-dd HH:mm:ss"), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")), null, TimeSpan.Zero, TimeSpan.FromMinutes(scanForErrorsRange));
            }*/
            System.Threading.Timer deleteOldTestsTimer = new System.Threading.Timer(deleteOldTests, null, TimeSpan.Zero, TimeSpan.FromHours(deleteOldTestsFrequency));
            System.Threading.Timer cleanDBTimer = new System.Threading.Timer(cleanDB, null, TimeSpan.Zero, TimeSpan.FromHours(cleanDBFreqency));
            Console.ReadLine();

            //log("Test Runner Application has Ended Successfully");
        }
        public static void log(string message)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(logFile))
                {
                    sw.WriteLine(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " - " + message);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static void Callback(object state)
        {           
            string testSettings = path + System.Environment.MachineName+".testsettings";
            string results = @"\\vs-control\E$\TestResults\";
            var loadTests = Directory.EnumerateFiles(path + @"WebAndLoadTestProject3\"+ System.Environment.MachineName, "*.loadtest").Select(System.IO.Path.GetFileName);

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.UseShellExecute = false;
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.RedirectStandardInput = true;
            startInfo.WorkingDirectory = @"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE";

            process.StartInfo = startInfo;
            process.Start();            
            process.StandardInput.WriteLine("REG ADD \"HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Lsa\" /V disabledomaincreds /T REG_DWORD /F /D 0");//used to enable email alerts via task scheduler.
            //checkForErrors(DateTime.Now.Subtract(new TimeSpan(0, scanForErrorsRange, 0)).ToString("yyyy-MM-dd HH:mm:ss"), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));//does not wait for previous step to execute. Effective but could be more efficient with fewer calls. 
            foreach (var loadTest in loadTests)
            {
                try
                {
                    if (!Directory.Exists(results + loadTest.Substring(0, (loadTest.Length - 9))))
                    {
                        Directory.CreateDirectory(results + loadTest.Substring(0, (loadTest.Length - 9)));
                    }
                    process.StandardInput.WriteLine("mstest /TestContainer:\"" + path + @"WebAndLoadTestProject3\" + System.Environment.MachineName + @"\" + loadTest + "\" /RunConfig:\"" + testSettings + "\"  /resultsfile:\"" + results + loadTest.Substring(0, (loadTest.Length - 9)) + @"\" + loadTest.Substring(0, (loadTest.Length - 9)) + " " + DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss")/*("ddMMyyyyHHmmss")*/ + ".trx\"");
                }
                catch
                {
                    if (!Directory.Exists(@"C:\TestResults\" + loadTest.Substring(0, (loadTest.Length - 9))))
                    {
                        Directory.CreateDirectory(@"C:\TestResults\" + loadTest.Substring(0, (loadTest.Length - 9)));
                    }
                    
                    process.StandardInput.WriteLine("mstest /TestContainer:\"" + path + @"WebAndLoadTestProject3\" + System.Environment.MachineName + @"\" + loadTest + "\" /RunConfig:\"" + testSettings + "\"  /resultsfile:\"C:\\TestResults\\" + loadTest.Substring(0, (loadTest.Length - 9)) + @"\" + loadTest.Substring(0, (loadTest.Length - 9)) + " " + DateTime.Now.ToString("yyyy-MM-dd HH_mm_ss")/*("ddMMyyyyHHmmss")*/ + ".trx\"");
                }                                
            }
            process.StandardInput.WriteLine("exit");

            Cleanup();
        }
        public static void deleteOldTests(object state)
        {
            if (System.Environment.MachineName == "VS-CONTROL")
            {
                string[] testFiles = Directory.GetFiles(@"E:\TestResults", "*.trx", SearchOption.AllDirectories);

                foreach (var file in testFiles)
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.CreationTime < DateTime.Now.AddDays(-1 * deleteTestsAfter))
                    {
                        fi.Delete();
                        log(fi.Name + " has been deleted as it is over " + deleteTestsAfter + " days old.");
                    }
                }
            }
            else
            {
                Console.WriteLine("#### Another machine is responsible for this process ####");
            }

        }
        static void Cleanup()
        {
            string[][] foldersArr = { Directory.GetDirectories(@"C:\Users\adm_elawrence3\AppData\Local\VSEQT\QTController"), Directory.GetDirectories(@"C:\Users\adm_elawrence3\AppData\Local\VSEQT\QTAgent"), Directory.GetDirectories(path + @"TestResults\") };

            if (System.Environment.MachineName == "VS-CONTROL")
            {
                var testResults = Directory.EnumerateFiles(path + @"TestResults\", "*.trx").Select(System.IO.Path.GetFileName);
                string newDirectory = @"E:\TestResults";

                if (!Directory.Exists(newDirectory))
                {
                    Directory.CreateDirectory(newDirectory);
                }

                foreach (var trx in testResults)
                {
                    string destination = newDirectory + @"\" + trx.Substring(0, (trx.Length - 24));
                    if (!Directory.Exists(destination))
                    {
                        Directory.CreateDirectory(destination);
                    }
                    try { File.Move(path + @"TestResults\" + trx, destination + @"\" + trx); }
                    catch { continue; }
                }

                string[] foldersArr2 = Directory.GetDirectories(newDirectory);
                foreach (var folders in foldersArr2)
                {
                    string[] foldersArr3 = Directory.GetDirectories(folders);
                    foreach (var folder in foldersArr3)
                    {
                        try { Directory.Delete(folder, true); }
                        catch { continue; }
                    }
                }
            }
            else
            {
                Console.WriteLine("#### Another machine is responsible for this process ####");
            }
            foreach (var folders in foldersArr)
            {
                foreach (var folder in folders)
                {
                    try { Directory.Delete(folder, true); }
                    catch { continue; }
                }
            }
           
        }
        public static void checkForErrors(string from, string to)
        {
            //string loadTestName = TestName.Substring(0, (TestName.Length - 9));           
            try
            {
                SqlConnection connection = new SqlConnection(connstr);
                DataTable table_Results = new DataTable();
                table_Results = new DataTable();
                table_Results.Columns.Add("Date", typeof(String));
                table_Results.Columns.Add("Time", typeof(String));
                table_Results.Columns.Add("TestName", typeof(String));
                table_Results.Columns.Add("Location", typeof(String));
                table_Results.Columns.Add("Transaction", typeof(String));
                table_Results.Columns.Add("TotalTime", typeof(Double));
                table_Results.Columns.Add("Outcome", typeof(int));

                connection.Open();
                //SqlCommand check = new SqlCommand("select TRe.LoadTestRunId,TRu.LoadTestName,TRo.ScenarioName,TRa.AgentName,TRu.StartTime,TRE.PageId,TRx.RequestUri,TRe.ResponseTime,TRe.Outcome from [LoadTestRepository].[dbo].[LoadTestPageDetail] TRe inner join [LoadTestRepository].[dbo].[LoadTestRun] TRu on TRe.LoadTestRunId = TRu.LoadTestRunId inner join [LoadTestRepository].[dbo].[WebLoadTestRequestMap] TRx on Tre.LoadTestRunId = TRx.LoadTestRunId and TRe.PageId = TRx.RequestId inner join [LoadTestRepository].[dbo].[LoadTestTestDetail] TRd on Tre.LoadTestRunId = TRd.LoadTestRunId inner join [LoadTestRepository].[dbo].[LoadTestScenario] TRo on Trd.LoadTestRunId = TRo.LoadTestRunId and TRo.ScenarioId = TRd.TestCaseId and TRe.TestDetailId = Trd.TestCaseId inner join [LoadTestRepository].[dbo].[LoadTestRunAgent] TRa on Trd.LoadTestRunId = TRa.LoadTestRunId and TRa.AgentId = TRd.AgentId and TRe.LoadTestRunId in (select LoadTestRunId from [LoadTestRepository].[dbo].[LoadTestRun] where Outcome = 'Completed') where TRe.Outcome='1' and TRu.LoadTestName='" + loadTestName + "' and TRu.StartTime BETWEEN '" + from + "' AND '" + to + "'", connection);
                /*SqlCommand check = new SqlCommand(@"select TRe.LoadTestRunId,
                                                           TRu.LoadTestName,
                                                           TRo.ScenarioName,
                                                           TRa.AgentName,
                                                           TRu.StartTime,
                                                           TRE.PageId,
                                                           TRx.RequestUri,
                                                           TRe.ResponseTime,
                                                           TRe.Outcome 
                                                           from [LoadTestRepository].[dbo].[LoadTestPageDetail] TRe 
                                                           inner join [LoadTestRepository].[dbo].[LoadTestRun] TRu 
                                                           on TRe.LoadTestRunId = TRu.LoadTestRunId 
                                                           inner join [LoadTestRepository].[dbo].[WebLoadTestRequestMap] TRx 
                                                           on Tre.LoadTestRunId = TRx.LoadTestRunId and TRe.PageId = TRx.RequestId 
                                                           inner join [LoadTestRepository].[dbo].[LoadTestTestDetail] TRd 
                                                           on Tre.LoadTestRunId = TRd.LoadTestRunId 
                                                           inner join [LoadTestRepository].[dbo].[LoadTestScenario] TRo 
                                                           on Trd.LoadTestRunId = TRo.LoadTestRunId and TRo.ScenarioId = TRd.TestCaseId 
                                                           and TRe.TestDetailId = Trd.TestCaseId 
                                                           inner join [LoadTestRepository].[dbo].[LoadTestRunAgent] TRa 
                                                           on Trd.LoadTestRunId = TRa.LoadTestRunId and TRa.AgentId = TRd.AgentId 
                                                           and TRe.LoadTestRunId in (select LoadTestRunId from [LoadTestRepository].[dbo].[LoadTestRun] 
                                                           where Outcome = 'Completed') where TRe.Outcome='1' 
                                                           and TRu.StartTime BETWEEN '" + from + "' AND '" + to + "'", connection);*/
                SqlCommand check = new SqlCommand(@"select TRe.LoadTestRunId,
                                            TRu.LoadTestName,
                                            TRe.ScenarioName, 
                                            TRe.TransactionName,
                                            TRu.StartTime,
                                            TRe.Average,
                                            TRx.Outcome
                                            from [LoadTestRepository].[dbo].[LoadTestTransactionResults] TRe 
                                            inner join [LoadTestRepository].[dbo].[LoadTestRun] TRu on TRe.LoadTestRunId=TRu.LoadTestRunId 
                                            inner join [LoadTestRepository].[dbo].[LoadTestScenario] TRs on TRe.ScenarioName = TRs.ScenarioName
                                            inner join[LoadTestRepository].[dbo].[LoadTestTestDetail] TRx on TRx.LoadTestRunId = TRe.LoadTestRunId and TRx.TestCaseId = TRs.ScenarioId and TRx.LoadTestRunId = TRs.LoadTestRunId
                                            where TRe.LoadTestRunId in (select LoadTestRunId from [LoadTestRepository].[dbo].[LoadTestRun] where Outcome='Completed')
                                            and TRu.StartTime BETWEEN '" + from + "' AND '" + to + "'", connection);
                SqlDataReader reader = check.ExecuteReader();
                DataTable temp = new DataTable();
                temp.Load(reader);
                
                
                foreach (DataRow r in temp.Rows)
                {
                    try
                    {
                        List<object> lo = r.ItemArray.ToList();
                        String date = lo[4].ToString().Remove(lo[4].ToString().IndexOf(' '));
                        String time = Convert.ToDateTime(lo[4]).AddMilliseconds(-(Convert.ToDateTime(lo[4]).Millisecond)).TimeOfDay.ToString();
                        String testName = lo[1].ToString();
                        String location = lo[2].ToString();
                        String transaction = lo[3].ToString();
                        Double totalTime = Convert.ToDouble(lo[5]);
                        int outcome = Convert.ToInt32(lo[6]);
                        table_Results.Rows.Add(date, time, testName, location, transaction, totalTime, outcome);

                        SqlCommand copy = new SqlCommand(@"INSERT INTO [LoadTestRepository].[dbo].[AppPerfMonDump]
                        VALUES (" + lo[0].ToString() + ",'" + testName + "','" + location + "','" + transaction + "','" + lo[4].ToString() + "','" + totalTime + "','" + outcome + "')", connection);
                        copy.ExecuteReader();
                        connection.Close();
                        connection.Open();
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }
                connection.Close();





                string query = "Outcome<>10";
                DataRow[] errors = table_Results.Select(query);
                // table_Results.Clear();      
                DataTable table_Results2 = new DataTable();
                table_Results2 = new DataTable();
                table_Results2.Columns.Add("Date", typeof(String));
                table_Results2.Columns.Add("Time", typeof(String));
                table_Results2.Columns.Add("TestName", typeof(String));
                table_Results2.Columns.Add("Location", typeof(String));
                table_Results2.Columns.Add("Transaction", typeof(String));
                table_Results2.Columns.Add("TotalTime", typeof(Double));
                table_Results2.Columns.Add("Outcome", typeof(int));

                foreach (DataRow row in errors)
                {
                    table_Results2.ImportRow(row);
                }

                if (table_Results2.Rows.Count > 0)
                {
                    table_Results2 = table_Results2.AsEnumerable().GroupBy(row => new {testname =  row.Field<string>("TestName"),location = row.Field<string>("Location")}).Select(group => group.First()).CopyToDataTable();                   
                }
                
                foreach (DataRow error in table_Results2.Rows)
                {
                    sendEmail("Performance monitoring has reported an error when connecting to " + error["TestName"].ToString() + " from " + error["Location"].ToString() + ". This error was reported at " + error["Time"].ToString() + " on " + error["Date"].ToString() + ".");
                    log("Performance monitoring has reported an error when connecting to " + error["TestName"].ToString() + " from " + error["Location"].ToString() + ".");
                }
            }
            catch (Exception e)
            {
                sendEmail("The SQL Load Test Repository produced an error: " + e + e.Message);
                log("The SQL Load Test Repository produced an error: " + e.Message);
            }
        }
        static void sendEmail(string message)
        {
            try
            {
                MailMessage mail = new MailMessage("QA_t.testy@snhu.edu", "e.lawrence3@snhu.edu");
                SmtpClient client = new SmtpClient();
                //  client.Port = 25;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = "smtp.snhu.edu";
                mail.Subject = "Performance Monitor Alert.";
                mail.Body = message;
                client.Send(mail);
            }
            catch(Exception e)
            {
                errorLog(e);
            }
        }
        static void errorLog(Exception ex)
        {
            string filePath = @"E:\TestRunner_LOG.txt";

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }
        }
        static void cleanDB(object state)
        {
            try
            {
                Console.WriteLine("Removing old tests from the database...");
                SqlConnection connection = new SqlConnection(connstr);
                connection.Open();

//                SqlCommand archive = new SqlCommand(@"");

                SqlCommand check = new SqlCommand(@"USE LoadTestRepository
                            DECLARE @LoadTestRunId int
                            DECLARE OldLoadTestsCursor CURSOR FOR
                                SELECT LoadTestRunId FROM LoadTestRun WHERE datediff(dd, StartTime, getdate()) > "+ removeTestFromDBafter + @"

                            OPEN OldLoadTestsCursor
                            FETCH NEXT FROM OldLoadTestsCursor INTO @LoadTestRunId

                            WHILE @@FETCH_STATUS = 0
                            BEGIN
                                EXEC Prc_DeleteLoadTestRun @LoadTestRunId
                                FETCH NEXT FROM OldLoadTestsCursor INTO @LoadTestRunId
                            END

                            CLOSE OldLoadTestsCursor
                            DEALLOCATE OldLoadTestsCursor", connection);
                check.CommandTimeout = 1800;
                SqlDataReader reader = check.ExecuteReader();
                DataTable temp = new DataTable();
                temp.Load(reader);
                connection.Close();
                Console.WriteLine("Database Cleaned!");
            }
            catch(Exception e)
            {
                sendEmail("The SQL Load Test Repository produced an error: " + e + e.Message);
                log("The SQL Load Test Repository produced an error: " + e.Message);
                System.Threading.Timer cleanDBTimer = new System.Threading.Timer(cleanDB, null, TimeSpan.Zero, TimeSpan.FromHours(1));
            }
        }
    }
}
