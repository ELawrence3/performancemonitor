﻿using PerfMonAlerts.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PerfMonAlerts.Controllers
{
    public class HomeController : Controller
    {
        private static DataTable descriptionTable_Results;
        private static DataTable table_Results;
        public static string connstr = @"Data Source=SQL2012-2A\ITS_APPS_TEST;Initial Catalog=LoadTestRepository;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False";
        public static IIdentity winId = System.Web.HttpContext.Current.User.Identity;
        public static string name = winId.Name;
        public static string email = name.Substring(5) + "@snhu.edu";
        // public static string email = @User.Identity.Name
        public static string adminEmail = "e.lawrence3@snhu.edu";

        public static string queryStringTestName = "";

        StringBuilder str = new StringBuilder();
        StringBuilder str1 = new StringBuilder();

        public ActionResult Index(string testName, string date)
        {

            if (date == null || date == "")
            {
                date = DateTime.Now.ToString("yyyy-MM-dd");
            }
            else {
                try
                {
                    date = date.Trim();
                    date = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
                }
                catch (Exception ex)
                {
                    date = DateTime.Now.ToString("yyyy-MM-dd");
                }
            }

            ViewBag.date = date;
            ViewBag.testName = testName;

            LoadDataTable(date, date);
            GridSelect(date, testName);

            DataRow[] result = getTestNames().Select();
            List<Test> tests = new List<Test>();
            foreach (var row in result)
            {
                tests.Add(new Test(row[0].ToString(), row[1].ToString(), row[2].ToString()));
            }

            ViewBag.tests = tests;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page. Neato";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = " Performance Monitoring Support and Related Inquiries.";


            return View();
        }
        public ActionResult Alerts()
        {
            
            ViewBag.email = HttpContext.User.Identity.Name.Substring(5) + "@snhu.edu"; // email;
            email = ViewBag.email = HttpContext.User.Identity.Name.Substring(5) + "@snhu.edu"; // email;
            
            DataRow[] result = getTestNames().Select();
            List<Test> tests = new List<Test>();
            foreach (var row in result)
            {
                try
                {
                    if (row[1].ToString() != "")
                        tests.Add(new Test(row[0].ToString(), row[1].ToString(), row[2].ToString()));                        
                }
                catch(Exception e)
                {
                    
                }
            }

            ViewBag.tests = tests;

            return View();
        }
        private DataTable getTestNames()
        {
            List<string> testNames = new List<string>();

            SqlConnection connection = new SqlConnection(connstr);
            descriptionTable_Results = new DataTable();
            descriptionTable_Results.Columns.Add("LoadTestName", typeof(String));
            descriptionTable_Results.Columns.Add("Description", typeof(String));
            descriptionTable_Results.Columns.Add("emailList", typeof(String));

            if (ViewBag.date == "" || ViewBag.date == null) {
                ViewBag.date = DateTime.Now.ToString("yyyy-MM-dd");
            }

            

            try
            {
                connection.Open();

                SqlCommand check = new SqlCommand();
                check = new SqlCommand(@"SELECT distinct [LoadTestRepository].[dbo].[LoadTestRun].[LoadTestName],tsd.[Description], tsd.[emailList]    
  FROM [LoadTestRepository].[dbo].[LoadTestRun]
  left join [LoadTestRepository].[dbo].[TestStepDetails] tsd on [LoadTestRepository].[dbo].[LoadTestRun].LoadTestName = tsd.LoadTestName
where  cast (datediff (day, 0, StartTime) as datetime) = '"+ ViewBag.date + "'", connection);
                SqlDataReader reader = check.ExecuteReader();

                descriptionTable_Results.Load(reader);

                connection.Close();

                return descriptionTable_Results;
            }
            catch (Exception e)
            {
                //nothing
                return new DataTable();
            }
        }
        public void LoadTestDescription(string testName)
        {
            try
            {
                String query = "LoadTestName = '" + testName + "'";

                DataRow[] result = table_Results.Select(query);

                if (result.Length == 0)
                {

                }
                else
                {

                }

                str.Append(@"<script type='text/javascript'>function openWin" + testName.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + @"(){myWindow=window.open('','Test Steps','toolbar=no,scrollbars=yes,resizable=yes,top=100,left=500,width=400,height=200'),myWindow.document.write('<H3>" + testName + " Test Steps</H3><button onclick=self.close()>Close</button><BR/><ol><li>" + result[0]["Description"].ToString().Replace(System.Environment.NewLine, "</li><li>") + @"</li></ol>')}</script>");
            }
            catch (Exception e)
            {
                str.Append(@"<script type='text/javascript'>function openWin" + testName.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + @"(){myWindow=window.open('','Test Steps','width=400,height=200'),myWindow.document.write('<H3>Error</H3><BR/><p>No description for this test available at this time.<BR/> Error Message:" + e.Message + @"</li></ol>');}</script>");
            }

        }

        [HttpPost]
        public ActionResult updateSubscription(bool[] isChecked, string[] testName)
        {
            List<string> subscribeList = new List<string>();
            List<string> unsubscribeList = new List<string>();

            int j = 0;
            int test = 0;
            for (int i = 0; i < isChecked.Length; i += 2) {
               
                String query = "LoadTestName = '" + testName[j] + "'";
                DataRow[] result = getTestNames().Select(query);
                if (result.Length > 0){

                    if (isChecked[i] && !result[0][2].ToString().Contains(email))//if subscription box checked, and user is not already subscribed
                    {
                        subscribeList.Add(testName[j]);
                    }
                    else if (!isChecked[i])//if subscription box unchecked
                    {
                        if (result[0][2].ToString().Contains(email))//if user is already subscribed
                                unsubscribeList.Add(testName[j]);
                        i--;//Checked boxes return 2 values (True, False). Unchecked boxes only return False. i-- on false moves counter one step back to account for this.
                    }
                }
                else
                {
                    test++;
                    if (!isChecked[i])
                        i--;
                }
                j++;
                ViewBag.error = "hello";                
            }


            if ((subscribeList.Count + unsubscribeList.Count) > 0) //If any changes have been requested
            {
                SmtpClient MySmtpClient = new SmtpClient("smtp.snhu.edu");
                MailMessage MyMail = new MailMessage(new MailAddress("PerfMon@snhu.edu"), new MailAddress(email));
                MyMail.Subject = "Your PerfMonAlerts Subscription has been updated!";

                MyMail.Body += "Hello " + email + "," + Environment.NewLine + Environment.NewLine
                    + "This email is to inform you of changes made to your SNHU Performance Monitor Alerts. The following changes have been made:"
                    + Environment.NewLine + Environment.NewLine ;

                if (subscribeList.Count > 0)//if new subscriptions requests have been recieved
                {
                    subscribe(subscribeList);

                    MyMail.Body += "You will now receive email alerts for:" + Environment.NewLine;
                    foreach (string tName in subscribeList)
                        MyMail.Body += tName + Environment.NewLine;
                }
                if (unsubscribeList.Count > 0)//if new unsubscription requests have been received
                {
                    unsubscribe(unsubscribeList);

                    MyMail.Body += Environment.NewLine + "You will no longer receive email alerts for:" + Environment.NewLine;
                    foreach (string tName in unsubscribeList)
                        MyMail.Body += tName + Environment.NewLine;
                }
                MyMail.Body += Environment.NewLine + "If you wish to make further changes please visit http://vs-control/PerfMonAlerts/Home/Alerts to update your subscription."
                    + Environment.NewLine + "For additional assistance you may contact the site's administrator directly at " + adminEmail + "."
                    + Environment.NewLine + Environment.NewLine + "All the best," + Environment.NewLine + Environment.NewLine + "The Southern New Hampshire University Quality Assurance Team";
                MySmtpClient.Send(MyMail);
            }
            return RedirectToAction("Index");
        }

        public void subscribe(List<string> subscribeList)
        {
            foreach (string testName in subscribeList)
            {
                try
                {
                    SqlConnection connection = new SqlConnection(connstr);
                    connection.Open();
                    SqlCommand check = new SqlCommand(@"UPDATE [LoadTestRepository].[dbo].[TestStepDetails]
                               set emailList += '" + email + @";'
                               where LoadTestName = '" + testName + "'", connection);
                    SqlDataReader reader = check.ExecuteReader();
                    connection.Close();
                }
                catch
                {

                }
            }
        }
        public void unsubscribe(List<string> unsubscribeList)
        {
            foreach (string testName in unsubscribeList)
            {
                try
                {
                    SqlConnection connection = new SqlConnection(connstr);
                    connection.Open();
                    SqlCommand check = new SqlCommand(@"UPDATE [LoadTestRepository].[dbo].[TestStepDetails]
                               set emailList = REPLACE(emailList,'" + email + @";','')
                               where LoadTestName = '" + testName + "'", connection);
                    SqlDataReader reader = check.ExecuteReader();
                    connection.Close();
                }
                catch
                {
                }
            }

        }




        //Begin PerfMon Charts functionality

        public void LoadDataTable(string startDate, string endDate)
        {
            string start = Convert.ToDateTime(startDate).ToString("yyyy-MM-dd");
            string end = Convert.ToDateTime(endDate).ToString("yyyy-MM-dd");

            SqlConnection connection = new SqlConnection(connstr);
            table_Results = new DataTable();
            // table_Results.Columns.Add("LoadTestRunID", typeof(Int32));
            table_Results.Columns.Add("LoadTestName", typeof(String));
            table_Results.Columns.Add("ScenarioName", typeof(String));
            table_Results.Columns.Add("TransactionName", typeof(String));
            table_Results.Columns.Add("StartTime", typeof(DateTime));
            table_Results.Columns.Add("Average", typeof(Double));
            table_Results.Columns.Add("Outcome", typeof(Byte));
            table_Results.Columns.Add("Description", typeof(String));
            

            try
            {
                connection.Open();

                SqlCommand check = new SqlCommand();
                check = new SqlCommand(@"select
		                            TRu.LoadTestName,
                                    TRs.ScenarioName, 
                                    TRe.TransactionName,
                                    TRu.StartTime,
                                    TRe.Average,
                                    TRx.Outcome,
                                    Trd.Description
                                    from [LoadTestRepository].[dbo].[LoadTestTransactionResults] TRe 
                                    inner join [LoadTestRepository].[dbo].[LoadTestTransactionSummaryData] TRsd on TRsd.LoadTestRunId = TRe.LoadTestRunId and TRsd.Average=TRe.Average
                                    inner join [LoadTestRepository].[dbo].[WebLoadTestTransaction] TRt on TRe.LoadTestRunId=Trt.LoadTestRunId and TRt.TransactionName=TRe.TransactionName and TRt.TransactionId=TRsd.TransactionId
                                    inner join [LoadTestRepository].[dbo].[LoadTestRun] TRu on TRe.LoadTestRunId=TRu.LoadTestRunId 
                                    inner join [LoadTestRepository].[dbo].[LoadTestScenario] TRs on TRe.ScenarioName = TRs.ScenarioName and TRs.LoadTestRunId = TRu.LoadTestRunId
                                    inner join [LoadTestRepository].[dbo].[LoadTestTestDetail] TRx on TRx.LoadTestRunId = TRe.LoadTestRunId and TRx.LoadTestRunId = TRs.LoadTestRunId and TRx.TestCaseId=TRt.TestCaseId
                                    inner join [LoadTestRepository].[dbo].[TestStepDetails] TRd on Trd.LoadTestName = TRu.LoadTestName
		                            where TRu.StartTime BETWEEN '" + start + " 00:00:00' AND '" + end + " 23:59:59'", connection);
                SqlDataReader reader = check.ExecuteReader();

                table_Results.Load(reader);

                connection.Close();

            }
            catch (Exception e)
            {
                Response.Write("<script type='text/javascript'>alert('OOPS!" + e.Message + "')</script>");
            }
        }


        public void GridSelect(string Date, string TestName)
        {
            DateTime dtDate = Convert.ToDateTime(Date);
            String query = "";

            //            viewingAll = false;
            if (TestName == "all")
                query = "StartTime >= '" + dtDate.Year + "-" + dtDate.Month + "-" + dtDate.Day + " 00:00:00.000' AND StartTime <= '" + dtDate.Year + "-" + dtDate.Month + "-" + dtDate.Day + " 23:59:59.999'";
            else
                query = "StartTime >= '" + dtDate.Year + "-" + dtDate.Month + "-" + dtDate.Day + " 00:00:00.000' AND StartTime <= '" + dtDate.Year + "-" + dtDate.Month + "-" + dtDate.Day + " 23:59:59.999' AND LoadTestName = '" + TestName + "'";
            //String query = "Date = '" + Date + "' AND LoadTestName = '" + TestName + "'";
            DataRow[] result = table_Results.Select(query);
            DataTable tableResults = new DataTable();


            //            tableResults.Columns.Add("LoadTestRunID", typeof(Int32));
            tableResults.Columns.Add("LoadTestName", typeof(String));
            tableResults.Columns.Add("ScenarioName", typeof(String));
            tableResults.Columns.Add("TransactionName", typeof(String));
            tableResults.Columns.Add("StartTime", typeof(DateTime));
            tableResults.Columns.Add("Average", typeof(Double));
            tableResults.Columns.Add("Outcome", typeof(Byte));
            //        tableResults.Columns.Add("Description", typeof(String));


            foreach (DataRow row in result)
            {
                tableResults.ImportRow(row);
            }

            BindChart(tableResults);
        }


        private void BindChart(DataTable dt)
        {
            
            object[] TestNameArray = dt.AsEnumerable().OrderBy(r => r.Field<String>("LoadTestName")).Select(r => r.Field<String>("LoadTestName")).ToList().Distinct().ToArray();
            DataTable temp = getTestNames();
            foreach (string tName in TestNameArray)
            { 
                    string query = "[LoadTestName] = '" + tName + "'";
                    string description = temp.Select(query)[0][2].ToString();
                    LoadTestDescription(tName);

                    object[] TransactionArray = dt.Select("[LoadTestName] = '" + tName + "'").AsEnumerable().Select(r => r.Field<String>("TransactionName")).ToList().Distinct().ToArray(); //dtScenarios.AsEnumerable().Select(r => r.Field<String>("TransactionName")).ToList().Distinct().ToArray();               
                    foreach (string transaction in TransactionArray)
                    {
                    try
                    {

                        str1.Append(@"<script type='text/javascript'>
                                    $(document).ready(function () {");

                        //Populate data to table

                        str1.Append("$('#tblChart').append(\"<div class='chart' id='chart_div" + tName.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + transaction.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + "' style='height:650px;width:100%;min-height:150px;padding-bottom:50px;overflow:visible!important;'></div>\");});</script>");

                            

                        var testsAsEnum = dt.Select("[LoadTestName] = '" + tName + "' AND [TransactionName] = '" + transaction + "'").AsEnumerable();
                        DateTime testsLastDT = testsAsEnum.Select(r => r.Field<DateTime>("StartTime")).ToList().Max();
                        DateTime testsFirstDT = testsAsEnum.Select(r => r.Field<DateTime>("StartTime")).ToList().Min();

                        double testsMinResponse = testsAsEnum.Select(r => r.Field<double>("Average")).ToList().Min();
                        double testsAvgResponse = testsAsEnum.Select(r => r.Field<double>("Average")).ToList().Average();


                        str.Append(@"<script type='text/javascript'>var myConfig" + tName.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + transaction.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + @"={type:'line',backgroundColor:'#FFF',legend:{adjustLayout:true,align:'center',verticalAlign:'bottom'},title:{adjustLayout:true,text:'Test: " + tName + "    Transaction: " + transaction + @"    Date: " + testsAsEnum.Select(r => r.Field<DateTime>("StartTime")).ToList().Min().ToString().Remove(testsAsEnum.Select(r => r.Field<DateTime>("StartTime")).ToList().Min().ToString().IndexOf(' ')) + @"'},plotarea:{margin:'dynamic 50 dynamic dynamic'},scaleX:{'values':'" + ConvertToUnixTimestamp(testsFirstDT) + ":" + ConvertToUnixTimestamp(testsLastDT) + @":1000','max-items':12,transform:{type: 'date',all: '%h:%i:%s'}},crosshairX:{exact: true,lineColor:'#000',scaleLabel:{borderRadius:2}},series:[");

                        int baseTransactionCount = 0;
                        List<KeyValuePair<int, int>> errorList = new List<KeyValuePair<int, int>>(); //plot(scenario), index/origional Value
                        List<KeyValuePair<int, ValuePair>> warningList = new List<KeyValuePair<int, ValuePair>>();//plot(scenario), index/origionalValue
                        double verifiedMaxResponse = 0;
                        object[] ScenarioArray = dt.Select("[LoadTestName] = '" + tName + "' AND [TransactionName] = '" + transaction + "'").OrderBy(r => r.Field<String>("LoadTestName")).AsEnumerable().Select(r => r.Field<String>("ScenarioName")).ToList().Distinct().ToArray();
                        for (int scenarioNum = 0; scenarioNum < ScenarioArray.Length; scenarioNum++)// string scenario in ScenarioArray)
                        {
                            string color = "";
                            if (ScenarioArray[scenarioNum].ToString() == "Campus")
                                color = "1874cd";
                            else if (ScenarioArray[scenarioNum].ToString() == "Elm")
                                color = "3cb371";
                            else if (ScenarioArray[scenarioNum].ToString() == "Millyard")
                                color = "b22222";


                            DataTable dtTransactions = new DataTable();

                            dtTransactions.Columns.Add("LoadTestName", typeof(String));
                            dtTransactions.Columns.Add("ScenarioName", typeof(String));
                            dtTransactions.Columns.Add("TransactionName", typeof(String));
                            dtTransactions.Columns.Add("StartTime", typeof(DateTime));
                            dtTransactions.Columns.Add("Average", typeof(Double));
                            dtTransactions.Columns.Add("Outcome", typeof(Byte));
                            dtTransactions.Columns.Add("Description", typeof(String));


                            String queryTransactions = "TransactionName = '" + transaction + "' and ScenarioName = '" + ScenarioArray[scenarioNum].ToString() + "' and LoadTestName = '" + tName + "'";

                            DataRow[] transactionsResult = dt.Select(queryTransactions);

                            foreach (DataRow row in transactionsResult)
                            {
                                dtTransactions.ImportRow(row);
                            }

                            baseTransactionCount = dtTransactions.Rows.Count;


                            str.Append(@"{values: [");
                            List<double> passingRows = new List<double>();

                            for (int row = 0; row < baseTransactionCount; row++)
                            {
                                if ((byte)dtTransactions.Rows[row]["Outcome"] != 10 || dtTransactions.Rows[row]["Average"].ToString() == "")
                                {
                                    dtTransactions.Rows[row]["Average"] = testsMinResponse;
                                    errorList.Add(new KeyValuePair<int, int>(scenarioNum, row));
                                }
                                else if ((double)dtTransactions.Rows[row]["Average"] > testsAsEnum.Select(r => r.Field<double>("Average")).ToList().Average() * 5)
                                    warningList.Add(new KeyValuePair<int, ValuePair>(scenarioNum, new ValuePair { index = row, origionalValue = (double)dtTransactions.Rows[row]["Average"], timestamp = (DateTime)dtTransactions.Rows[row]["StartTime"] }));
                                else
                                    passingRows.Add((double)dtTransactions.Rows[row]["Average"]);

                                str.Append(@"[" + ConvertToUnixTimestamp((DateTime)dtTransactions.Rows[row]["StartTime"]) + "," + dtTransactions.Rows[row]["Average"] + "]");
                                if (row < baseTransactionCount - 1)
                                    str.Append(",");
                            }

                            if (passingRows.Count <= 0)
                                passingRows.Add(0.0f);

                            double avgResponseTime = passingRows.Average();//.Sum() / passingRows.Count;
                            if (passingRows.Max() > verifiedMaxResponse)
                                verifiedMaxResponse = passingRows.Max();

                            str.Append(@"],lineColor:'#" + color + @"','text':'" + ScenarioArray[scenarioNum].ToString() + @"',marker:{visible: false,backgroundColor:'#000'}}");//colors the dots
                            if (scenarioNum < ScenarioArray.Length - 1)
                                str.Append(",");
                        }
                        str.Append(@"],'labels':[{'text':'Test Steps','url':'javascript:openWin" + tName.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + @"()','font-color':'#29A2CC','text-decoration':'underline','font-size':'18','x':'1%','y':'1%'}");
                        for (int warningID = 0; warningID < warningList.Count; warningID++)
                            str.Append(@",{'text':'WARNING','background-color':'#FFC700','offset-y':'-500px','hook':'scale:name=scale-x;value=" + ConvertToUnixTimestamp(warningList[warningID].Value.timestamp) + @"'}");
                        for (int errorID = 0; errorID < errorList.Count; errorID++)
                            str.Append(@",{'text':'ERROR','background-color':'#FF0000','callout': true,'callout-height': '10px','callout-width': '15px','offset-y':'-30px','hook':'node:plot=" + errorList[errorID].Key + ";index=" + errorList[errorID].Value + @"'}");

                        str.Append(@"],scaleY:{'values':'" + testsMinResponse + ":" + verifiedMaxResponse + @":" + (verifiedMaxResponse - testsMinResponse) / 10 + @"','max-items':10,guide:{lineStyle:'solid'},label:{text: 'Response Time (sec.)'},markers:[{type:'line',range: [23],lineColor:'#c62828',lineStyle:'dashed',label:{text:'Average',placement: 'right'}}]}}
$(document).ready(function () {zingchart.render({id: 'chart_div" + tName.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + transaction.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + @"',data: myConfig" + tName.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + transaction.Replace('-', '_').Replace(' ', '_').Replace('.', '_') + @",height:'100%',width:'100%'});})</script>");
                            
                    }
                    catch(Exception e)
                    {
                        ViewBag.alerts += tName + " failed! ";
                        str.Clear();
                        str1.Clear();
                        continue;
                        
                    }
                    ViewBag.mainlt += str1.ToString();//.Replace('*', '"');
                    ViewBag.lt += str.ToString();//.Replace('*', '"');
                    str.Clear();
                    str1.Clear();
                }
            }            
        }
        
        /// <summary>
        ///Utility Functions
        /// </summary>
        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalMilliseconds);
        }
    }       
struct ValuePair
    {
        public int index;
        public double origionalValue;
        public DateTime timestamp;
    }
}