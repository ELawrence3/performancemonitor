﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PerfMonAlerts.Models
{
    public class Test
    {
        private string name { get; set; }
        private string description { get; set; }
        private string emailList { get; set; }

        public Test(string Name, string Description, string Email)
        {
            name = Name;
            description = Description;
            emailList = Email;            
        }

        public string getName()
        {
            return name;
        }

        public string getDescription()
        {
            return description;
        }
        public string getEmailList()
        {
            return emailList;
        }
    }
}